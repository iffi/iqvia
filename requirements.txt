celery==4.3.0
flask==1.0.2
Flask-SQLAlchemy==2.4.0
flask-migrate==2.5.2
flask-restful==0.3.7
python-dotenv==0.10.3
redis==3.2.1