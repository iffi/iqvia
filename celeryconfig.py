CELERY_IMPORTS = ('contacts.tasks',)

DELETE_CONTACTS_EVERY = 1

CELERYBEAT_SCHEDULE = {
    'add-contacts': {
        'task': 'contacts.tasks.random_contacts',
        # Every 15 seconds
        'schedule': 15,
        'args': (DELETE_CONTACTS_EVERY,)
    },
}