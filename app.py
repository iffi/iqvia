import os

from flask import Flask

import celeryconfig
from config import config
from core import api, migrate, db, celery_app


def create_app(config_name):
    flask_app = Flask(__name__)

    set_environment(config_name, flask_app)
    init_api(flask_app)

    return flask_app


def init_api(flask_app):
    from contacts.api import ContactListResource, ContactResource
    api.add_resource(ContactListResource, '/contacts/', endpoint='contacts')
    api.add_resource(ContactResource, '/contacts/<string:user>', endpoint='contact')
    api.init_app(flask_app)


def set_environment(config_name, flask_app):
    flask_app.config.from_object(config[config_name])

    db.init_app(flask_app)

    # SQLite does not respect FK cascade delete constraints
    # unless 'pragma foreign_keys=ON' is enabled
    # every time a new db connection is open
    def _fk_pragma_on_connect(dbapi_con, con_record):
        dbapi_con.execute('pragma foreign_keys=ON')

    from sqlalchemy import event
    event.listen(db.get_engine(flask_app), 'connect', _fk_pragma_on_connect)

    # initialize migrations
    from contacts import models
    migrate.init_app(flask_app, db)


def init_celery(flask_app):
    celery_config = dict(main=flask_app.import_name,
                         result_backend=flask_app.config['CELERY_RESULT_BACKEND'],
                         broker_url=flask_app.config['CELERY_BROKER_URL'])

    celery_app.conf.update(flask_app.config)
    celery_app.config_from_object(celeryconfig)
    celery_app.conf.update(celery_config)

    class ContextTask(celery_app.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery_app.Task = ContextTask


app = create_app(os.getenv('FLASK_ENV') or 'default')
init_celery(app)


if __name__ == '__main__':
    app.run()
