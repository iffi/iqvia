from sqlalchemy.exc import IntegrityError

from core import db
from contacts.models import Contact, Email
from tests.base import IqviaTests


class ContactsTestCase(IqviaTests):

    def test_not_null_constraints(self):
        # test contact must have a username
        contact = Contact(username=None)
        db.session.add(contact)
        self.assertRaises(IntegrityError, db.session.commit)
        db.session.rollback()

        # this works
        contact.username = 'Horray!'
        db.session.add(contact)
        contact.emails.append(Email(address='i@am.in', contact_id=contact.id))
        db.session.commit()
        assert contact in db.session

    def test_uniqueness_constraints(self):
        contact = Contact(username='i_am_first')
        db.session.add(contact)
        contact.emails.append(Email(address='i@am.first', contact_id=contact.id))
        db.session.commit()

        duplicate_user = Contact(username='i_am_first')
        db.session.add(duplicate_user)

        #
        # no duplicate usernames allowed
        #
        self.assertRaises(IntegrityError, db.session.commit)
        db.session.rollback()

        #
        # no duplicate emails allowed
        #
        duplicate_user.username = 'me_again'
        duplicate_user.emails.append(Email(address=contact.emails[0].address,
                                           contact_id=duplicate_user.id))
        db.session.add(duplicate_user)

        self.assertRaises(IntegrityError, db.session.commit)
        db.session.rollback()

    def test_contact_with_multiple_emails(self):
        contact = Contact(username='i_am_first')
        db.session.add(contact)
        contact.emails.append(Email(address='i@am.first', contact_id=contact.id))
        contact.emails.append(Email(address='me@again.com', contact_id=contact.id))
        db.session.commit()

        emails = {'i@am.first', 'me@again.com'}
        self.assertEqual(len(contact.emails), 2)
        self.assertSetEqual(set(contact.email_addresses), emails)

        #
        # test delete email does not delete contact
        #
        db.session.delete(contact.emails[0])
        db.session.commit()

        contact_exists = Contact.query.filter_by(username='i_am_first').one()
        self.assertEqual(len(contact_exists.emails), 1)

        #
        # Test contact deletion deletes all of its emails
        #
        db.session.delete(contact_exists)
        db.session.commit()
        emails = Email.query.all()
        self.assertEqual(emails, [])