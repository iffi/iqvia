import unittest

from flask import current_app

from app import app
from core import db


class IqviaTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.app = app
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.browser = cls.app.test_client()

    def setUp(self) -> None:
        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    @classmethod
    def tearDownClass(cls) -> None:
        super().tearDownClass()
        cls.app_context.pop()

    def test_app_exists(self):
        self.assertFalse(current_app is None)

    def test_app_in_testing(self):
        self.assertTrue(current_app.config['TESTING'])
