import json

from core import db
from contacts.models import Contact, Email
from tests.base import IqviaTests


class ContactsApiTestCase(IqviaTests):

    def setUp(self) -> None:
        super(ContactsApiTestCase, self).setUp()
        self.first = Contact(username='i_am_first', firstname='first', surname='one')
        self.second = Contact(username='i_am_second', firstname='second', surname='one')
        db.session.add_all((self.first, self.second))
        self.first.emails.append(Email(address='i@am.first'))
        self.second.emails.append(Email(address='i@am.second'))
        db.session.commit()

    def test_get_contact(self):
        #
        # Test successful get of an existing contact by username
        #
        resp = self.browser.get('/contacts/{}'.format(self.first.username))
        result = resp.json

        self.assertEqual(resp.status_code, 200)
        self._result_equals_contact_instance(self.first, result)

        #
        # Test successful get of an existing contact by email
        #
        resp = self.browser.get('/contacts/{}'.format(self.first.email_addresses[0]))
        result = resp.json

        self.assertEqual(resp.status_code, 200)
        self._result_equals_contact_instance(self.first, result)

        #
        # Test attempt to get of a nonexistent contact
        #
        resp = self.browser.get('/contacts/blabla')

        self.assertEqual(resp.status_code, 404)

    def test_delete_contact(self):
        #
        # Test successful deletion of an existing contact
        #
        resp = self.browser.delete('/contacts/{}'.format(self.first.username))

        self.assertEqual(resp.status_code, 204)
        all_contacts = Contact.query.all()

        self.assertEqual(len(all_contacts), 1)
        self.assertEqual(all_contacts[0], self.second)

        #
        # Test attempt for deletion of a nonexistent contact
        #
        resp = self.browser.delete('/contacts/4')

        self.assertEqual(resp.status_code, 404)

    def test_patch_contact(self):
        #
        # Test successful username and multiple emails update
        #
        resp = self.browser.patch('/contacts/{}'.format(self.first.username),
                                  data=json.dumps({'username': 'new_name', 'emails': ['a@.b.c', 'd@e.f']}),
                                  content_type='application/json')

        result = resp.json

        self.assertEqual(resp.status_code, 200)
        self._result_equals_contact_instance(self.first, result)

        #
        # Test update nonexistent contact
        #
        resp = self.browser.patch('/contacts/blabla',
                                  data=json.dumps({'username': 'new_name'}),
                                  content_type='application/json')

        self.assertEqual(resp.status_code, 404)

        #
        # Test set username to null fails
        #
        resp = self.browser.patch('/contacts/{}'.format(self.first.username),
                                  data=json.dumps({'username': None}),
                                  content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self._assert_constraint_error('username', 'Must not be null!', result['message'])

        #
        # Test set emails to null fails
        #
        resp = self.browser.patch('/contacts/{}'.format(self.first.username),
                                  data=json.dumps({'emails': None}),
                                  content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self._assert_constraint_error('emails', 'Must not be null!', result['message'])

        #
        # Test emails must respect the regex format
        #
        resp = self.browser.patch('/contacts/{}'.format(self.first.username),
                                  data=json.dumps({'emails': ['aaa.33']}),
                                  content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self._assert_constraint_error('emails', r'Value does not match pattern: ".+\@.+\..+"', result['message'])

        #
        # Test set username to uniqueness
        #
        resp = self.browser.patch('/contacts/{}'.format(self.first.username),
                                  data=json.dumps({'username': self.second.username}),
                                  content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self.assertEqual('Unique fields duplication.', result['message'])

        #
        # Test set emails to uniqueness
        #
        resp = self.browser.patch('/contacts/{}'.format(self.first.username),
                                  data=json.dumps({'emails': self.second.email_addresses[0]}),
                                  content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self.assertEqual('Unique fields duplication.', result['message'])

    def test_put_contact(self):
        #
        # Test successful username and multiple emails update
        #
        resp = self.browser.put('/contacts/{}'.format(self.first.username),
                                data=json.dumps({'username': 'new_name', 'emails': ['my@new.mail', 'my@second.email'],
                                                 'firstname': 'new', 'surname': 'one'}),
                                content_type='application/json')

        result = resp.json

        self.assertEqual(resp.status_code, 200)
        self._result_equals_contact_instance(self.first, result)

        #
        # Test update nonexistent contact
        #
        resp = self.browser.put('/contacts/blabla',
                                data=json.dumps({'username': 'new_name', 'emails': ['my@new.mail'],
                                                 'firstname': 'new', 'surname': 'one'}),
                                content_type='application/json')

        self.assertEqual(resp.status_code, 404)

        #
        # Test set username to null fails
        #
        resp = self.browser.put('/contacts/{}'.format(self.first.username),
                                data=json.dumps({'username': None, 'emails': ['my@new.mail'],
                                                 'firstname': 'new', 'surname': 'one'}),
                                content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self._assert_constraint_error('username', 'Must not be null!', result['message'])

        #
        # Test set emails to null fails
        #
        resp = self.browser.put('/contacts/{}'.format(self.first.username),
                                data=json.dumps({'username': 'new_name', 'emails': None,
                                                 'firstname': 'new', 'surname': 'one'}),
                                content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self._assert_constraint_error('emails', 'Must not be null!', result['message'])

        #
        # Test emails must respect the regex format
        #
        resp = self.browser.put('/contacts/{}'.format(self.first.username),
                                data=json.dumps({'username': 'new_name', 'emails': ['aaa.33'],
                                                 'firstname': 'new', 'surname': 'one'}),
                                content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self._assert_constraint_error('emails', r'Value does not match pattern: ".+\@.+\..+"', result['message'])

        #
        # Test set username to uniqueness
        #
        resp = self.browser.put('/contacts/{}'.format(self.first.username),
                                data=json.dumps({'username': self.second.username, 'emails': ['my@new.mail'],
                                                 'firstname': 'new', 'surname': 'one'}),
                                content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self.assertEqual('Unique fields duplication.', result['message'])

        #
        # Test set email to uniqueness
        #
        resp = self.browser.put('/contacts/{}'.format(self.first.username),
                                data=json.dumps({'username': 'new_name', 'emails': [self.second.email_addresses[0]],
                                                 'firstname': 'new', 'surname': 'one'}),
                                content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self.assertEqual('Unique fields duplication.', result['message'])

    def test_get_all_contacts(self):
        resp = self.browser.get('/contacts/')
        result = resp.json

        self.assertEqual(resp.status_code, 200)
        self.assertTrue(len(result), 2)
        self._result_equals_contact_instance(self.first, result[0])
        self._result_equals_contact_instance(self.second, result[1])

    def test_post_contact(self):
        #
        # Test successfully post a new contact with multiple emails
        #
        resp = self.browser.post('/contacts/',
                                 data=json.dumps({'username': 'new_kid_on_the_block',
                                                  'emails': ['i@am.new', 'my@second.mail'],
                                                  'firstname': 'number', 'surname': 'three'}),
                                 content_type='application/json')

        result = resp.json

        self.assertEqual(resp.status_code, 201)
        third = Contact.query.filter_by(username='new_kid_on_the_block').first()
        self._result_equals_contact_instance(third, result)

        #
        # Test fail posting new contact with existing username
        #
        resp = self.browser.post('/contacts/',
                                 data=json.dumps({'username': self.first.username, 'emails': ['i@amnot.new'],
                                                  'firstname': 'number', 'surname': 'three'}),
                                 content_type='application/json')

        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self.assertEqual('Contact already exists. Try using PUT or PATCH  method instead', result['message'])

        #
        # Test fail posting new contact with existing username
        #
        resp = self.browser.post('/contacts/',
                                 data=json.dumps({'username': 'see_me_fail', 'emails': third.email_addresses[0],
                                                  'firstname': 'number', 'surname': 'three'}),
                                 content_type='application/json')

        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self.assertEqual('Contact already exists. Try using PUT or PATCH  method instead', result['message'])

        #
        # Test username must not be null
        #
        resp = self.browser.post('/contacts/',
                                 data=json.dumps({'username': None, 'emails': ['i@am.new'],
                                                  'firstname': 'number', 'surname': 'three'}),
                                 content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self._assert_constraint_error('username', 'Must not be null!', result['message'])

        #
        # Test emails must not be null
        #
        resp = self.browser.post('/contacts/',
                                 data=json.dumps({'username': 'hi_there', 'emails': None,
                                                  'firstname': 'number', 'surname': 'three'}),
                                 content_type='application/json')
        result = resp.json

        self.assertEqual(resp.status_code, 400)
        self._assert_constraint_error('emails', 'Must not be null!', result['message'])

    def _result_equals_contact_instance(self, contact: Contact, result: dict) -> None:
        emails = result.pop('emails')
        for k, v in result.items():
            self.assertEqual(v, contact.__dict__[k])

        self._result_emails_equal_contact_emails(contact.emails, emails)

    def _result_emails_equal_contact_emails(self, contact_email: list, result: list) -> None:
        self.assertEqual(len(contact_email), len(result))
        result_email_addresses = {mail['address'] for mail in result}
        contact_email_addresses = {mail.address for mail in contact_email}
        self.assertSetEqual(contact_email_addresses, result_email_addresses)

    def _assert_constraint_error(self, field, expected_msg, error):
        self.assertEqual(error[field], expected_msg)
