# IQVIA test project

## Requirements
To run the project you need 

* Python3 version 3.6 or higher
* pip
* redis server 

## Project Setup Guide

* Install required libraries with 
```
pip install -r requirements.txt
```
* Set up the database: 
```
flask db upgrade
```

## Running The Project
### Start The Server
`flask run`
### Start Celery
`celery worker -A app.celery_app -B --loglevel=info`
### Run The Tests
`FLASK_ENV=testing python3 -m unittest discover`

