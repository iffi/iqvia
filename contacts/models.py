import datetime

from core import db


class Contact(db.Model):
    __tablename__ = 'contacts'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Unicode(128), unique=True, nullable=False, index=True)
    firstname = db.Column(db.Unicode(128))
    surname = db.Column(db.Unicode(128))
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    emails = db.relationship('Email',
                             backref=db.backref('contact'),
                             cascade='save-update, merge, delete, delete-orphan')

    @property
    def email_addresses(self):
        addresses = [email.address for email in self.emails]
        return addresses

    def __repr__(self) -> str:
        return f"""Contact with id: {self.id}, username: {self.username}, 
                  firstname: {self.firstname}, surname: {self.surname}, emails:{self.email_addresses}"""


class Email(db.Model):
    __tablename__ = 'emails'

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.Unicode(128), unique=True, nullable=False, index=True)
    contact_id = db.Column(db.Integer, db.ForeignKey('contacts.id', ondelete='CASCADE'),
                           nullable=False, index=True)
