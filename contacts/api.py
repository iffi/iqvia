from flask_restful import Resource,  fields, reqparse, inputs, marshal_with, abort
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from core import db
from contacts import Contact, Email

EMAIL_REGEX = inputs.regex(r'.+\@.+\..+')

contact_fields = {
    'id': fields.Integer,
    'username': fields.String,
    'emails': fields.List(
        fields.Nested(
            {'id': fields.Integer,
             'address': fields.String,
             'contact_id': fields.Integer
             })),
    'firstname': fields.String,
    'surname': fields.String,
}

parser = reqparse.RequestParser()
parser.add_argument('username', type=str, required=True, nullable=False)
parser.add_argument('emails', type=EMAIL_REGEX,  action='append', required=True, nullable=False)
parser.add_argument('firstname', nullable=True)
parser.add_argument('surname', nullable=True)

# email_parser = reqparse.RequestParser()
# email_parser.add_argument('address', type=EMAIL_REGEX, required=True, nullable=False, location=('emails',))

# Patch method allows partial updates. And the parser is configured in a way to process only subset of all arguments
# e.g. if the request payload contains only {'username': 'new_name'}
# and the rest of the arguments are missing from the payload
patch_parser = reqparse.RequestParser()
patch_parser.add_argument('username', type=str, nullable=False, store_missing=False)
patch_parser.add_argument('emails', type=EMAIL_REGEX,  action='append', nullable=False, store_missing=False)
patch_parser.add_argument('firstname', nullable=True, store_missing=False)
patch_parser.add_argument('surname', nullable=True, store_missing=False)


class ContactResource(Resource):
    def _get_or_except(self, username):
        try:
            contact = Contact.query.filter_by(username=username).one()
        except NoResultFound:
            abort(404, message="Contact {} doesn't exist".format(username))
        return contact

    def _get_or_except_by_identifier(self, identifier):
        """Find contact by username OR by email"""
        try:
            contact = Contact.query.filter_by(username=identifier).first()
            contact = contact or Email.query.filter_by(address=identifier).one().contact
        except NoResultFound:
            abort(404, message="Contact {} doesn't exist".format(identifier))
        return contact

    @marshal_with(contact_fields)
    def get(self, user):
        contact = self._get_or_except_by_identifier(user)
        return contact

    def delete(self, user):
        contact = self._get_or_except(user)
        db.session.delete(contact)
        db.session.commit()
        return {}, 204

    @marshal_with(contact_fields)
    def put(self, user):
        """
        The PUT method uses the request URI to supply a modified version of the requested resource
        which replaces the original version of the resource
        """
        parsed_args = parser.parse_args()
        contact = self._get_or_except(user)
        emails = parsed_args.pop('emails')
        for k, v in parsed_args.items():
            setattr(contact, k, v)
        try:
            db.session.add(contact)
            update_emails(contact, emails)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            abort(400, message="Unique fields duplication.")
        return contact, 200

    @marshal_with(contact_fields)
    def patch(self, user):
        """
        Perform a partial update to the Contact object
        """
        parsed_args = patch_parser.parse_args()
        contact = self._get_or_except(user)
        emails = parsed_args.pop('emails', [])

        for k, v in parsed_args.items():
            setattr(contact, k, v)
        try:
            db.session.add(contact)
            update_emails(contact, emails)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            abort(400, message="Unique fields duplication.")
        return contact, 200


class ContactListResource(Resource):
    @marshal_with(contact_fields)
    def get(self):
        contacts = Contact.query.all()
        return contacts

    @marshal_with(contact_fields)
    def post(self):
        parsed_args = parser.parse_args()
        emails = parsed_args.pop('emails')
        contact = Contact(**parsed_args)
        try:
            db.session.add(contact)
            update_emails(contact, emails)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            abort(400, message="Contact already exists. Try using PUT or PATCH  method instead")
        return contact, 201


def update_emails(contact, emails):
    new_emails = []
    for email in emails:
        new_emails.append(Email(address=email, contact_id=contact.id))
    contact.emails = new_emails
