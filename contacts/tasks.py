import datetime
import logging
import random
import string

from sqlalchemy import or_

from app import celery_app
from core import db
from contacts import Contact, Email

log = logging.getLogger(__name__)


@celery_app.task
def random_contacts(minutes):
    delete_older_contacts(minutes)
    create_random_contact()


def create_random_contact():
    username = ''.join([random.choice(string.ascii_letters) for _ in range(4)])
    contact = Contact(username=username, firstname=f'f{username}',
                      surname=f's{username}')
    try:
        db.session.add(contact)
        contact.emails.append(Email(address=f'{username}@example.com'))
        contact.emails.append(Email(address=f'{username}@mymail.com'))
        db.session.commit()
        log.info(f"Added {contact}")
    except Exception as e:
        db.session.rollback()
        log.exception(e, exc_info=True)


def delete_older_contacts(minutes):
    minutes_ago = datetime.datetime.utcnow() - datetime.timedelta(minutes=minutes)
    try:
        deleted = Contact.query.filter(or_(Contact.created_at < minutes_ago, Contact.created_at.is_(None))).delete()
        log.info(f'Deleted {deleted} contacts')
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        log.exception(e, exc_info=True)
